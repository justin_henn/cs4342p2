#Justin Henn
#Random stump forest using Adaboost

import random
import math

random.seed(1)

#class for nodes in stump forest
class Node(object):
    def __init__(self, threshold):
        self.thresh = threshold
        self.alpha = 0
        self.left = None
        self.right = None
        self.leaf = False
        self.predict = None

#data sets, info gain for top level, and weights for AdaBoost
my_list = [.5, 3.0, 4.5, 4.6, 4.9, 5.2, 5.3, 5.5, 7.0, 9.5]
my_list_class = [-1, -1, 1, 1, 1, -1, -1, 1, -1, -1]
test_list = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
my_weights  = [.1, .1, .1, .1, .1, .1, .1, .1, .1, .1]
parent_ig = (-1 * (6/10) * math.log(6/10, 2)) - ((4/10) * math.log(4/10, 2))
num_round = 1

#get the prediction for the node
def get_predict(neg,pos):
    
    if neg > pos:
        prediction = -1
    else:
        prediction = 1        
    return prediction

#calculate the info gain for the node
def get_info_gain(threshold, train_list, train_class):
    left_side = []
    right_side = []
    neg_left = 0
    neg_right = 0
    pos_left = 0
    pos_right = 0
    ig_neg_left = 0.0
    ig_pos_left = 0.0
    ig_neg_right = 0.0
    ig_pos_right = 0.0
    
    for i in (train_list):
        if i < threshold:
            left_side.append(train_class[train_list.index(i)])
            if train_class[train_list.index(i)] == -1:
                neg_left += 1
            else:
                pos_left += 1
        else:
            right_side.append(train_class[train_list.index(i)])
            if train_class[train_list.index(i)] == -1:
                neg_right += 1
            else:
                pos_right += 1

    if pos_left != 0:
        ig_pos_left = (-1 * (pos_left / len(left_side)) * math.log(float(pos_left) / len(left_side), 2))
    if neg_left != 0:
        ig_neg_left = (-1 * (neg_left / len(left_side)) * math.log(float(neg_left) / len(left_side), 2))
    if pos_right != 0:
        ig_pos_right = (-1 * (pos_right / len(right_side)) * math.log(float(pos_right) / len(right_side), 2))
    if neg_right != 0:
        ig_neg_right = (-1 * (neg_right / len(right_side)) * math.log(float(neg_right) / len(right_side), 2))
    
    ig = ((len(left_side) / len(train_list))*(ig_pos_left + ig_neg_left)) + ((len(right_side) / len(train_list))*(ig_pos_right + ig_neg_right))
    
    left_predict = get_predict(neg_left, pos_left)
    right_predict = get_predict(neg_right, pos_right)
    ig = parent_ig - ig
    return ig, left_predict, right_predict

#function to get entropy and class for node
def get_entropy(train_list, train_class):    
    max_threshold =  0
    max_ig = -.11111111
    max_left_predict = 0
    max_right_predict = 0
    for i in range (len(train_list)-1):
        if train_class[i] != train_class[i+1]:
            threshold = (train_list[i] + train_list[i+1]) / 2.0
            ig, left_predict, right_predict = get_info_gain(threshold, train_list, train_class)
            if ig > max_ig:
                max_ig = ig
                max_threshold = threshold 
                max_left_predict = left_predict
                max_right_predict = right_predict
    return max_threshold, max_left_predict, max_right_predict

#do predictions on the full traing set
def predict_train(tree):
    error_total = 0
    error_list = []
    for i in (my_list):
        if i < tree.thresh:
            if tree.left.predict != my_list_class[my_list.index(i)]:
                error_total += 1
                error_list.append(i)
        else:
            if tree.right.predict != my_list_class[my_list.index(i)]:
                error_total += 1
                error_list.append(i)
    return error_total, error_list

#calculate the alpha
def get_alpha(epsilon):   
    return (1/2) * math.log((1-epsilon)/(epsilon)) 

#update the weights
def update_weights(epsilon, error_list):
    for i in (my_list):
        if i in error_list:
            my_weights[my_list.index(i)] = my_weights[my_list.index(i)] / (2 *epsilon)
        else:
            my_weights[my_list.index(i)] = my_weights[my_list.index(i)] / (2* (1 - epsilon))

#build the decision tree stumps
def build_tree():
    while True:
        error_list = []
        epsilon = 0
        train_list = (random.choices(my_list, weights = my_weights, k = 10))
        train_list.sort()
        train_class = []
        for i in (train_list):
            train_class.append(my_list_class[my_list.index(i)])
        threshold, left_predict, right_predict = get_entropy(train_list, train_class)
        tree = Node(threshold)
        tree.left = Node(None)
        tree.right = Node(None)
        tree.left.leaf = True
        tree.right.leaf = True
        tree.right.predict = right_predict
        tree.left.predict = left_predict
        
        error_total, error_list = predict_train(tree)
        for i in (error_list):
            epsilon += my_weights[my_list.index(i)]
        alpha = get_alpha(epsilon)
        tree.alpha = alpha
        
            
        if epsilon < .5:
            global num_round
            print("Round: %d" % num_round)
            print("W = ", end = '')
            print(my_weights)
            print("X = ", end = '')
            print(train_list)
            print("Y = ", end = '')
            print(train_class)
            print("Spilt Point: %.3f \t Left Class: %d \t Right Class: %d" % (tree.thresh, tree.left.predict, tree.right.predict))
            print("Epsilon: %.3f \t Alpha: %.5f" % (epsilon, tree.alpha))
            print("\n")
            update_weights(epsilon, error_list)
            num_round += 1
            break
        else:
            for i in range(len(my_weights)):
                my_weights[i] = .1
    return tree

#make predictions on the test set    
def predict_test_set(lst, data):
    total_alpha = 0
    for i in range(len(lst)):
        if data < lst[i].thresh:
            total_alpha += lst[i].alpha * lst[i].left.predict
        else:
            total_alpha += lst[i].alpha * lst[i].right.predict
    if total_alpha < 0.0:
        return -1
    else:
        return 1
    
#call the build tree function to make the forest
lst = [build_tree() for i in range(10)]

#run the forest on the test set
test_predict = [0]*10
for i in range(len(test_list)):
    test_predict[i] = predict_test_set(lst, test_list[i])

#print test set reults
print("Ensemble Test Results:")
print(test_predict)
